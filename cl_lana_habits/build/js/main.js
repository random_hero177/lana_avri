// JavaScript Document
$(document).ready(function(){
	$('.j-order').click(function(e) {
		e.preventDefault();
		var curId = $(this).attr('href'),
			$form = $(curId).offset().top;
			$("html, body").animate({scrollTop : $form}, 666);
		return false;
	});
    $('.open_modal').click( function(event){
		
        event.preventDefault();
		
        var modalName = $(this).attr('href');
        $('.overlay').removeClass('__vis');
        $(modalName).addClass('__vis');
        $('body').css('overflow-x', 'hidden');
    });
    $('.modal-close').click( function(event){
        $(this).parents('.overlay').removeClass('__vis');
        $('body').css('overflow-x', 'hidden');
    });
    $('.overlay').click( function(event){
        $(this).removeClass('__vis');
        $('body').css('overflow-x', 'hidden');
    });
    $('.modal-window').click(function(e) {
        e.stopPropagation();
    });

});
